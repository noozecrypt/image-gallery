<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\utils.php';


    session_start();

    $comment_id;
    $comment;
    $user_id;
    $image_id;
    $is_root;
    $parent_comment_id;


    init();
    comment();

    #for debug only
    include dirname(__FILE__,2).'\utils\latest_response.php';

    function init() {
        global $comment_id;
        global $comment;
        global $user_id;
        global $image_id;
        global $is_root;
        global $parent_comment_id;

        $comment = 'looks great!';
        $image_id = 'ef625eb3-52ab-4ac4-abf8-9501e46324be';
        $is_root = false;
        $parent_comment_id = 'ffd80d54-e266-4a23-89ca-9db081790918';
        $comment_id = gen_uuid();
        $user_id = get_uuid_user();
        if ($_SESSION['status'] == 0){
            comment_fail();
        }

    }

    
    function comment(){
        global $conn;
        global $comment_id;
        global $comment;
        global $user_id;
        global $image_id;
        global $is_root;
        global $parent_comment_id;

        if(!$is_root){
            $sql = "BEGIN";

            $result = pg_query($conn, $sql); 
            if (!$result){
                comment_fail(); return;
            }


            $sql = "insert into comment values(
                '$comment_id','$comment', 
                '$user_id', '$image_id',
                false,'$parent_comment_id')";
            $res1 = pg_query($conn, $sql);

            $sql = "update comment set no_of_children = 
            (select count(*) from comment where 
            parent_comment_id = '$parent_comment_id') 
            where comment_id = '$parent_comment_id'";
            $res2 = pg_query($conn, $sql);

            if (!$res1 || !$res2 || pg_affected_rows($res1) == 0 || pg_affected_rows($res2) == 0 ){
                $sql = "ROLLBACK";
                pg_query($conn, $sql); 
                comment_fail(); return;
            }else{
                $sql = "COMMIT";
                $result = pg_query($conn, $sql); 
                if (!$result){
                    comment_fail(); return;
                }else{
                    comment_success(); return;
                }
                
            }
        }else{
            $sql = "insert into comment values(
                '$comment_id','$comment', 
                '$user_id', '$image_id')";
            $result = pg_query($conn, $sql);
            if (!$result || pg_affected_rows($result) == 0){
                comment_fail(); return;
            }else {
                comment_success(); return;
            }
        }

    }
   

    function comment_fail(){
        on_failure('comment addition failed!', 0);
    }

    function comment_success(){
        on_success('comment added successfully!', 0);
    }    
    
?>