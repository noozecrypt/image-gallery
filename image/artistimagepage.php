<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../css/nav.css">
    </head>
    <body>
        <?php 
            session_start();
  
            include dirname(__FILE__,2).'\utils\latest_response.php';
            require dirname(__FILE__,2).'\utils\logged_in.php';
            require dirname(__FILE__,2).'\utils\image_utils.php';
            include dirname(__FILE__,2).'\utils\nav.html'; 

            if (!is_artist($_SESSION['logged_in'])){
                redirect('/home.php');
            }

            $image_name = 'texture_3';
            $result = get_image($image_name);
            printout($result);

            $artist_name = 'fireFist';
            $result = get_images($artist_name);
            printout($result);

            $cat_name = 'space';
            $result = get_image_by_cat($cat_name);
            printout($result);

            $result = get_category($cat_name);
            printout($result);

            $username = 'theWhale';
            $user_id = get_user_uuid($username);
            if ($_SESSION['status'] == 1){
                $result = user_hearts($user_id);
                printout($result);
            }
           
            $image_name = 'texture_3';
            echo 'times_marked_fav('.$image_name.') : '.times_marked_fav($image_name).'<br><br>';
        
            $image_id = 'ef625eb3-52ab-4ac4-abf8-9501e46324be';
            $result = get_root_comments($image_id);
            printout($result);

            $parent_comment_id = '5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4';
            $result = get_comments($parent_comment_id);
            printout($result);
        ?>
    </body>
</html>