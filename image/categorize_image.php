<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\image_utils.php';

    session_start();

    #replace with form data or AJAX request data from uploadimagepage.php
    $image_name = 'texture_3';
    $cat_name = 'cars';
    $cat_id;
    $image_id;

    init();
    categorize();


    function init(){
        global $image_name;
        global $cat_name;
        global $image_id;
        global $cat_id;
        $artist_id;
        $artist_id_from_image;

        $image_name = 'texture_3';
        $cat_name = 'cars';
        $artist_id = get_uuid_user();
        if ($_SESSION['status'] == 0){
            categorize_fail();
        }
        $image_id = get_image_uuid($image_name);
        if ($_SESSION['status'] == 0){
            categorize_fail();
        }
        $cat_id = get_cat_uuid($cat_name);
        if ($_SESSION['status'] === 0){
            categorize_fail();
        }
        $artist_id_from_image = get_artist_uuid($image_name);
        if ($_SESSION['status'] === 0){
            categorize_fail();
        }

        if ($artist_id != $artist_id_from_image){
            categorize_fail();
        }


    }

    function categorize_fail(){
       on_failure('image categorization fail!', '/image/uploadimagepage.php');
    }

    function categorize_success(){
       on_success('image categorization success !', '/image/uploadimagepage.php');
    }    
    
    function categorize() {
        global $conn;
        global $image_id;
        global $cat_id;

        $sql = "insert into categorization values ('$image_id',
        '$cat_id')";
    
        $result = pg_query($conn, $sql); 
        if (!$result || pg_affected_rows($result) === 0){
            categorize_fail();
        }else {
            categorize_success();
        }
    }


    
?>