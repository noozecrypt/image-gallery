<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\utils.php';

    session_start();
    
    $cat_id;
    $cat_name;
    $cat_desc;

    init();
    create();

    function init(){
        global $cat_name;
        global $cat_desc;
        global $cat_id;

        $cat_id = gen_uuid();
        $cat_desc = 'super cars';
        $cat_name = 'cars';
    }

    function create(){
        global $conn;
        global $cat_name;
        global $cat_desc;
        global $cat_id;

        $sql = "insert into Category values('$cat_id',
        '$cat_name', '$cat_desc')";
        
        $result = pg_query($conn, $sql); 
        if (!$result || pg_affected_rows($result) === 0){
            create_fail();
        }else {
            create_success();
        }
    }

    function create_fail(){
        on_failure('category creation failure!', '/image/uploadimagepage.php');
    }

    function create_success(){
        on_success('category creation success!', '/image/uploadimagepage.php');
    }        
?>