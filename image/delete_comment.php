<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\utils.php';


    session_start();

    $comment_id;
    $user_id;
    $is_root;
    $parent_comment_id;


    init();
    delete_comment();

    #for debug only
    include dirname(__FILE__,2).'\utils\latest_response.php';

    function init() {
        global $comment_id;
        global $user_id;
        global $is_root;
        global $parent_comment_id;

        $comment_id = 'ffd80d54-e266-4a23-89ca-9db081790918';
        $is_root = true;
        $parent_comment_id = 'ffd80d54-e266-4a23-89ca-9db081790918';
        $user_id = get_uuid_user();
        if ($_SESSION['status'] == 0){
            delete_fail();
        }

    }

    
    function delete_comment(){
        global $conn;
        global $comment_id;
        global $user_id;
        global $is_root;
        global $parent_comment_id;

        $sql = "BEGIN";

        $result = pg_query($conn, $sql); 
        if (!$result){
            delete_fail(); return;
        }


        $sql = "delete from comment 
        where comment_id = '$comment_id' 
        and user_id = '$user_id'";
        $result = pg_query($conn, $sql);
        if (!$result || pg_affected_rows($result) === 0){        
            delete_fail(); return;
        }

        if (!$is_root){
            $sql = "update comment set no_of_children = 
            (select count(*) from comment where 
            parent_comment_id = '$parent_comment_id') 
            where comment_id = '$parent_comment_id'";
            $result = pg_query($conn, $sql);
            if (!$result || pg_affected_rows($result) == 0){
                $sql = "ROLLBACK";
                $result = pg_query($conn, $sql); 
                delete_fail(); return;
            }
        }

        $sql = "COMMIT";
        $result = pg_query($conn, $sql); 
        if (!$result){
            delete_fail(); return;
        }else{
            delete_success(); return;
        }

    }
   

    function delete_fail(){
        on_failure('comment deletion failed!', 0);
    }

    function delete_success(){
        on_success('comment deletion success!', 0);
    }    
    
?>