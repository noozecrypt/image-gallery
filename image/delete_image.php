<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\utils.php';
    require dirname(__FILE__,2).'\utils\image_utils.php';


    session_start();

    $image_name;
    $artist_id;

    $target_dir = dirname(__FILE__,2).'\uploads\images\\';
    $target_file;

    init();
    delete();

    function init(){
        global $image_name;
        global $artist_id;
        global $target_dir;
        global $target_file;

        $image_name = 'texture_3';
        $artist_id = get_uuid_user();
        if ($_SESSION['status'] == 0){
            #echo 1;
            delete_fail();
        }
        $image_format = get_image_format($image_name);
        if ($_SESSION['status'] == 0){
            #echo 2;
            delete_fail();
        }    

        $target_file = $target_dir.'\\'.$image_name.$image_format;
    }

    function delete(){
        global $conn;
        global $image_name;
        global $artist_id;
        global $target_file;

        $sql = "delete from image where name = '$image_name' and artist_id = '$artist_id'";
    
        $result = pg_query($conn, $sql); 
        if (!$result || pg_affected_rows($result) === 0){
            #echo 3;
            delete_fail();
        }else {
            unlink($target_file); 
            delete_success();

        }
    }
   

    function delete_fail(){
       on_failure('image deletion failed!', '/image/artistimagepage.php');
    }

    function delete_success(){
        on_success('image deletion success!', '/image/artistimagepage.php');
    }        
?>