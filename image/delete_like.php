<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\utils.php';


    session_start();

    $comment_id;
    $user_id;

    init();
    delete_like();

    #for debug only
    include dirname(__FILE__,2).'\utils\latest_response.php';

    function init() {
        global $comment_id;
        global $user_id;

        $comment_id = '5602b3ea-cb45-11e9-a67f-252ae2dbcce4';
        $user_id = get_uuid_user();
        if ($_SESSION['status'] == 0){
            delete_like_fail();
        }

    }

    
    function delete_like(){
        global $conn;
        global $comment_id;
        global $user_id;

        $sql = "BEGIN";

        $result = pg_query($conn, $sql); 
        if (!$result){
            delete_like_fail(); return;
        }


        $sql = "delete from commentlikes 
        where comment_id = '$comment_id' 
        and user_id = '$user_id';";
        $res1 = pg_query($conn, $sql);

        $sql = "update comment set times_liked = 
            (select count(*) from commentlikes where 
            comment_id = '$comment_id') 
            where comment_id = '$comment_id'";
        $res2 = pg_query($conn, $sql);

        if (!$res1 || !$res2 || pg_affected_rows($res1) == 0 || pg_affected_rows($res2) == 0 ){
            $sql = "ROLLBACK";
            pg_query($conn, $sql); 
            delete_like_fail(); return;
        }else{
            $sql = "COMMIT";
            $result = pg_query($conn, $sql); 
            if (!$result){
                    delete_like_fail(); return;
            }else{
                    delete_like_success(); return;
            }
        }

    }
   

    function delete_like_fail(){
        on_failure('comment like delete failed!', 0);
    }

    function delete_like_success(){
        on_success('comment like delete success!', 0);
    }    
    
?>