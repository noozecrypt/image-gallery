<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\utils.php';
    require dirname(__FILE__,2).'\utils\image_utils.php';

    session_start();


    $image_name = 'texture_3';
    $image_id;

    init();
    uncategorize();

    function init(){
        global $image_name;
        global $image_id;
        $artist_id;
        $artist_id_from_image;

        $artist_id = get_uuid_user();
        if ($_SESSION['status'] == 0){
            uncategorize_fail();
        }
        $image_id = get_image_uuid($image_name);
        if ($_SESSION['status'] == 0){
            uncategorize_fail();
        }
        $artist_id_from_image = get_artist_uuid($image_name);
        if ($_SESSION['status'] === 0){
            uncategorize_fail();
        }

        if ($artist_id != $artist_id_from_image){
            uncategorize_fail();
        }

    }


    function uncategorize_fail(){
       on_failure('image uncategorization failed', '/image/uploadimagepage.php');
    }

    function uncategorize_success(){
       on_success('image uncategorization success', '/image/uploadimagepage.php');
    }    
    
    function uncategorize() {
        global $conn;
        global $image_id;

         $sql = "delete from categorization
         where image_id = '$image_id'";
    
        $result = pg_query($conn, $sql); 
        if (!$result || pg_affected_rows($result) === 0){
            uncategorize_fail();
        }else {
            uncategorize_success();
        }
    }


    
?>