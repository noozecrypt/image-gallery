<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\image_utils.php';
    require dirname(__FILE__,2).'\utils\utils.php';

    session_start();

    $image_name;
    $image_id;
    $user_id;

    init();
    unheart_image();

    #for debug only
    include dirname(__FILE__,2).'\utils\latest_response.php';

    function init() {
        global $image_name;
        global $image_id;
        global $user_id;

        $image_name = 'texture_3';
        $image_id = get_image_uuid($image_name);
        if ($_SESSION['status'] == 0){
            unheart_fail();
        }
        $user_id = get_uuid_user();
        if ($_SESSION['status'] == 0){
            unheart_fail();
        }
    }


    
    function unheart_image(){
        global $conn;
        global $user_id;
        global $image_id;
        #start transaction
        $sql = "BEGIN";

        $result = pg_query($conn, $sql); 
        if (!$result){
            heart_fail();
        }

        $sql = "delete from heart where user_id = '$user_id'
        and image_id = '$image_id'";
        $res1 = pg_query($conn, $sql);
        
        $sql = "update image set times_marked_fav = (select count(*)
        from heart where image_id = '$image_id') 
        where image_id = '$image_id'";
        $res2 = pg_query($conn, $sql);

        if (!$res1 || !$res2 || pg_affected_rows($res1) == 0 || pg_affected_rows($res2) == 0 ){
            $sql = "ROLLBACK";
            pg_query($conn, $sql); 
            unheart_fail();
        }else{
            $sql = "COMMIT";
            pg_query($conn, $sql); 
            unheart_success();
        }

    }
   

    function unheart_fail(){
        on_failure('image unheart failure!', 0);
    }

    function unheart_success(){
        on_success('image unheart success!', 0);
    }    
    
  


    
?>