<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\utils.php';

    session_start();

    init();
    update();
    
    $old_image_name;
    $new_image_name;
    $desc;
    $artist_id;

    function init(){
        global $old_image_name;
        global $new_image_name;
        global $desc;
        global $artist_id;

        $old_image_name = 'texture_3';
        $new_image_name = 'texture_3';
        $desc = "tree trunk texture";

        $artist_id = get_uuid_user();
        if ($_SESSION['status'] == 0){
            update_fail();
        }
    }

    function update(){
        global $conn;
        global $old_image_name;
        global $new_image_name;
        global $desc;
        global $artist_id;

        $sql = "update image set name = '$new_image_name', description = '$desc' where
        name = '$old_image_name' and artist_id = '$artist_id'";
        
        $result = pg_query($conn, $sql); 
        if (!$result || pg_affected_rows($result) === 0){
            update_fail();
        }else {
            update_success();
        }

    }

   

    function update_fail(){
        on_failure('image update failure!', '/image/artistimagepage.php');
    }

    function update_success(){
        on_success('image update success!', '/image/artistimagepage.php');
    }        
?>