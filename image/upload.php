<html>
    <body>
    <?php
$target_dir = dirname(__FILE__,2).'\uploads\images\\';
$target_file = $target_dir.basename($_FILES["fileToUpload"]["name"]);
echo $target_file;

/* $_FILES - An associative (key-value pairs) of items 
 * uploaded to the current script via the HTTP POST method.
 * 
 * basename() - Given a string containing path to a file or directory, this function will return the trailing name component.
 * e.g echo basename("/etc/passwd"); return "passwd"
 */

$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
//Check if image file is a actual image or fake image
if (isset($_POST["submit"])) {
    $image_details = getimagesize($_FILES["fileToUpload"]["tmp_name"]);

    /* getimagesize() - will determine the size of any supported given image file and 
     * return the dimensions along with the file type and a height/width text string to be used 
     * inside a normal HTML IMG tag and the correspondent HTTP content type.
     *
     * returns an array with up to 7 elements. 
     *  Index 0 and 1 contains respectively the widht and the height of the image.
     *  Index 2 is one of the IMAGETYPE_XXX constants indicating the type of the image.
     *  Index 3 is the text string with the correct height="yyy" width="xxx" string for use in IMG tag.
     *  mime - MIME type of the image.
     *  channels will be 3 for RGB and $ for CMYK
     *  bits is the no. bits for each color
     * 
     * return, false on failure.
     */ 
    if ($check === false) {
        on_failure('File is not an image!','/users/uploadimagepage.php');
        
    }

    //Check if fiie already exists
    if (file_exists($target_file)){
        on_failure('A file with the given image name already exists', '/users/uploadimagepage.php');   
    }

    //Check file size 
    if ($_FILES["fileToUpload"]["size"] > 20000000){
        on_failure('A file with the given image name already exists', '/users/uploadimagepage.php');
    }

    //Allow certain file formats
    if ($imageFileType != "jpg" &&
         $imageFileType != "png" && 
         $imageFileType != "jpeg" && 
         $imageFileType!="gif") {
        on_failure('A file with the given image name already exists', '/users/uploadimagepage.php');       
    }


    //Check $uploadOk
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    }else {
        $fileName = $_FILES["fileToUpload"]["tmp_name"];
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "The file ". basename($_FILES["fileToUpload"]["name"]). " has been uploaded.";
        }else {
            echo "Sorry, there was an error uploading your file.";
        }
    }

    $file_loc = '\\'.pathinfo(dirname(__FILE__,2))['basename'].'\\uploads\\images\\'.basename($_FILES["fileToUpload"]["name"]);
    echo $file_loc;
    echo '<img src="'. $file_loc .'" width="' .($check[0]/4).'" height="'.($check[1]/4). '" >';

    
}
?>
</body>

</html>


