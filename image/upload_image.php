<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/nav.css">
    <link rel="stylesheet" type="text/css" href="../css/imagefeed.css">
    <link rel="stylesheet" type="text/css" href="../css/form.css">
</head>
<body>
<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\utils.php';
    include dirname(__FILE__,2).'\utils\nav.html'; 


    session_start();

    $target_dir = dirname(__FILE__,2).'\uploads\images\\';
    $target_file;
    

    $image_id;
    $artist_id;
    $image_name;
    $desc;
    $image_size;
    $image_width;
    $image_height;
    $image_format;
    
    init();
    upload();

     #for debug only
     include dirname(__FILE__,2).'\utils\latest_response.php';

    function init(){
        global $image_id;
        global $image_name;
        global $desc;
        global $image_size;
        global $image_width;
        global $image_height;
        global $image_format;
        global $artist_id;
        global $target_dir;
        global $target_file;

        $image_name = 'texture_3';
        $desc = 'tree bark texture';

        if (isset($_POST["submit"])) {
            $image_details = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            
            if ($image_details === false) {
                on_failure('File is not an image, file upload failed!','/image/uploadimagepage.php');
            }

            if ($_FILES["fileToUpload"]["size"] > 20000000){
                on_failure('File too big; file upload failed!', '/image/uploadimagepage.php');
            }

            $imageFileType = strtolower(pathinfo(basename($_FILES["fileToUpload"]["name"]), PATHINFO_EXTENSION));

            if ($imageFileType != "jpg" &&
            $imageFileType != "png" && 
            $imageFileType != "jpeg" && 
            $imageFileType!="gif") {
                on_failure('Only files with a jpg,jpeg,png, or gif file extensions are allowed, file upload failed!', '/users/uploadimagepage.php');       
            }

            $image_size = ($_FILES["fileToUpload"]["size"]/1000000);
            $image_width = $image_details[0];
            $image_height = $image_details[1];
            $image_format = '.'.$imageFileType;

        }else{
            upload_fail();
        }
       
        $image_id = gen_uuid();
        $artist_id = get_uuid_user();
        if ($_SESSION['status'] == 0){
            upload_fail();
        }

        $target_file = $target_dir.'\\'.$image_name.$image_format;   

    }

    function upload(){
        global $conn;
        global $image_id;
        global $image_name;
        global $desc;
        global $image_size;
        global $image_width;
        global $image_height;
        global $image_format;
        global $artist_id;
        global $target_file;

        $sql = "BEGIN";
        $result = pg_query($conn, $sql); 

        if (!$result){
            upload_fail();
            return;
        }

        $sql = "insert into image values ('$image_id',
            '$artist_id','$image_name','$desc',$image_size,$image_width,$image_height,'$image_format')";
    
        $result = pg_query($conn, $sql); 
        if (!$result || pg_affected_rows($result) === 0){
            $sql = "ROLLBACK";
            $result = pg_query($conn, $sql); 
            upload_fail();
            return;
        }

        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            $sql = "COMMIT";
            $result = pg_query($conn, $sql); 
            if (!$result){
                upload_fail();
                return;
            }
            upload_success();
        }else {
            $sql = "ROLLBACK";
            $result = pg_query($conn, $sql); 
            upload_fail();
            return;
        }

        while ($image_width > 1000){
            $image_width /= 2;
            $image_height /= 2;
        }


        $file_loc = '\\'.pathinfo(dirname(__FILE__,2))['basename'].'\\uploads\\images\\'.$image_name.$image_format;

        echo '<div class = picture><img src="'. $file_loc .'" width="' .($image_width).'" height="'.($image_height). '" ></div>
        <br><div class = container style = "width:1000px"><fieldset>The file'.basename($_FILES['fileToUpload']['name']).'has been uploaded.</fieldset></div>';
    }

    function upload_fail(){
       # on_failure('Sorry, there was an error uploading your file; file upload failed!', '/image/uploadimagepage.php');
       on_failure('Sorry, there was an error uploading your file; file upload failed!', 0);
    }

    function upload_success(){
       # on_success("The file ". basename($_FILES['fileToUpload']['name'])."has been uploaded.", '/image/uploadimagepage.php');
       #on_success("The file ". basename($_FILES['fileToUpload']['name'])." has been uploaded.", 0);
    }        
?>
</body>
</html>