<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../css/nav.css">
        <link rel="stylesheet" type="text/css" href="../css/form.css">
    </head>
    <body>
        <?php 
            session_start();
            include dirname(__FILE__,2).'\utils\latest_response.php';
            require dirname(__FILE__,2).'\utils\logged_in.php';
            include dirname(__FILE__,2).'\utils\nav.html'; 
            

            # use $_SESSION['status'] to check upload success or failure status  

            if (!is_artist($_SESSION['logged_in'])){
                redirect('/home.php');
            }

        ?>
        <div class = container style='width:600px'>
        <form action="upload_image.php" method="post" enctype="multipart/form-data">
        <fieldset>
            Select image to upload:
            <input type="file" name="fileToUpload" id="fileToUpload">
            <input type="Submit" name="submit" value="Upload Image">
        </fieldset>
        </form>
        </div>
    </body>
</html>