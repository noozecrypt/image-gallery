--artists
insert into enduser values ('ac4baada-50d5-4bb2-afd3-3370b9e11b37',
'fireFist',crypt('56Hoe%6^', gen_salt('bf')),'fireFist1997@gmail.com');

insert into enduser values ('b30710b7-c7c5-4c1f-8abe-12c19117878d',
'dirtMetal',crypt('dIrtM54@', gen_salt('bf')),'dirt_Metal78@gmail.com');

insert into enduser values ('e83a070d-d350-43d0-8daa-90333fe620ae',
  'greenBinary',crypt('grey010', gen_salt('bf')),'greenBinary010@gmail.com');

insert into enduser values ('12b4b766-22db-481b-80b5-2e1b2570c7c9',
'elementalGene',crypt('eneg76RNA', gen_salt('bf')),'ele_67_gene@gmail.com');

insert into artist (artist_id, description)
  values ('ac4baada-50d5-4bb2-afd3-3370b9e11b37', 'abstract image artist');

insert into artist (artist_id, description)
  values ('b30710b7-c7c5-4c1f-8abe-12c19117878d', 'pattern creation artist');

insert into artist (artist_id, description)
  values ('e83a070d-d350-43d0-8daa-90333fe620ae', 'photo artist');

insert into artist (artist_id, description)
  values ('12b4b766-22db-481b-80b5-2e1b2570c7c9', 'unique artist');

--create artist
insert into artist (artist_id, description)
  values ('42e80878-fb68-40d2-ac4e-f0b8a8e83228', 'magic artist');

--get artist
select * from enduser join artist on user_id = artist_id
where username = 'whiteShark';

--update artist
update artist set description = 'vector artist' where artist_id = (select
  user_id from enduser where username = 'whiteShark');
