insert into categorization values ('ef625eb3-52ab-4ac4-abf8-9501e46324be',
  'cc44da0c-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe',
  'cc44da0c-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('bf0ea9e6-1d15-4aae-bd95-d153ce355308',
  'cc44da0c-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('e368ac03-7a3d-40da-9404-97af459ae630',
  'cc44da0c-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('5291c3f1-ab37-4910-bd44-23d5e4dfeead',
  'cc44da0c-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('a803eb74-c418-41b7-9566-bd3e9d32a1b6',
  'cc44ddb8-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('a5f15e85-52b4-431e-b6bf-3f53ab912d29',
  'cc44ddb8-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('4ba49de3-28a9-45ce-9b34-eba9236d0af5',
  'cc44ddb8-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('dd23e3d8-a1d3-4d08-ac16-b647a974afff',
  'cc44ddb8-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('556e142e-37e5-4afc-9b8c-f37d017651bc',
  'cc44f352-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('723abd2f-eb2b-483c-b57f-0a711e93aba7',
  'cc44f352-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('fc84db2d-33c9-4a9b-af79-b8d95066a5ec',
  'cc44f352-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('b60e488f-44db-4d5b-9428-4fc0a85da94d',
  'cc44f352-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('9c1bea84-7041-41f1-81b3-93c63d828049',
  'cc44f352-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('362fe098-e131-4c3c-8f85-5c8ea8ed8955',
  'cc44e060-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('7ffc886d-56c5-422e-b86a-60bf1278bff0',
  'cc44e060-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('94aee617-1164-48dc-a102-e80908d5e054',
  'cc44e060-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('02d5f345-64cb-4cc8-aa64-ec69be5046f1',
  'cc44e060-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('5e381be8-3f88-4887-bb37-0014f5e445aa',
  'cc44f0b4-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('dc052bd7-e351-4a3e-8cf6-bf762192c743',
  'cc44f0b4-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('ce382b97-a96e-4327-ab11-7a500363f2c3',
  'cc44f0b4-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('18742dd7-a7d7-4fb1-92d9-3c5ae22ce26c',
  'cc44f0b4-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('6000db08-f9ea-45e1-9f60-783676cd2084',
  'cc44e2ea-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('7ff3b71a-5e73-4e45-8617-ab4850488857',
  'cc44e2ea-ab63-11e8-98d0-529269fb1459');

insert into categorization values ('26571422-336c-4b92-92c9-fcd056eb8f9a',
  'cc44e2ea-ab63-11e8-98d0-529269fb1459');

-- create categorization

insert into categorization values ('0b88ef60-994f-403c-8b3a-29b6e584009c',
  'cc44e2ea-ab63-11e8-98d0-529269fb1459');

-- remove categorization

delete from categorization
  where image_id = '0b88ef60-994f-403c-8b3a-29b6e584009c';
