insert into Category values('cc44da0c-ab63-11e8-98d0-529269fb1459',
  'abstract', 'abstract images');

insert into Category values('cc44ddb8-ab63-11e8-98d0-529269fb1459',
  'earth', 'images of planet earth');

insert into Category values('cc44e060-ab63-11e8-98d0-529269fb1459',
  'photography', 'photo images');

insert into Category values('cc44e2ea-ab63-11e8-98d0-529269fb1459',
  'texture', 'a lot of different');

insert into Category values('cc44f0b4-ab63-11e8-98d0-529269fb1459',
  'space', 'images of the cosmos');

insert into Category values('cc44f352-ab63-11e8-98d0-529269fb1459',
  'pattern', 'patterns');


--create category
insert into Category values('cc44f352-ab63-1165-98d0-529269fb1459',
  'cars', 'super cars');

-- get category
select * from category where name = 'cars';

--update category
update category set name = 'vans', description = 'super vans' where
  name = 'cars';

-- delete category
delete from category where name = 'vans';
