insert into comment values(
	'5602b3ea-cb45-11e9-a32f-2a2ae2dbcce4',
	'This is so perfect, definitely going to be new wallpaper', 
	'9c012fb7-34f2-4b37-b23c-dfb16c3a1092', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be');

insert into comment values(
	'5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4',
	'Very creative!, anyone know what software is used to create illustrations like these?', 
	'b80b408a-c5c6-4017-8c10-d32f165626b0', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be');


insert into comment values(
	'5602b3ea-cb43-11e9-a32f-2a2ae2dbcfe4',
	'Any vector editing software say, Adobe illustrator, will do', 
	'19230494-7261-4f7d-98d3-7d6f267b3980', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be',
    false,'5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4');
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4') 
	where comment_id = '5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4';

insert into comment values(
	'5602b3ea-cb43-41e9-a32f-2a2ae2dbcfe4',
	'Thanks, will check it out', 
	'b80b408a-c5c6-4017-8c10-d32f165626b0', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be',
    false,'5602b3ea-cb43-11e9-a32f-2a2ae2dbcfe4');
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-cb43-11e9-a32f-2a2ae2dbcfe4') 
	where comment_id = '5602b3ea-cb43-11e9-a32f-2a2ae2dbcfe4';

insert into comment values(
	'5602b3ea-cb43-11e9-a32f-2a2ae2dbcfe7',
	'Any free to use software will be great', 
	'b80b408a-c5c6-4017-8c10-d32f165626b0', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be',
    false,'5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4');
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4') 
	where comment_id = '5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4';

insert into comment values(
	'5602b3ea-cb43-11g9-a39f-2a2ae2dbcfe7',
	'check out Inkscape, it is free', 
	'9c012fb7-34f2-4b37-b23c-dfb16c3a1092', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be',
    false,'5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4');
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4') 
	where comment_id = '5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4';

insert into comment values(
	'5502b3ea-cb45-11e9-a32f-2a2ae2gbcce4',
	'great work, subscribing to you', 
	'24c011c3-d798-4434-a5d5-32f973acc4ad', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be');	

insert into comment values(
	'5602b3ea-cb45-11e9-a32f-2a2ae2gbcce4',
	'just perfect in my oled display', 
	'9c012fb7-34f2-4b37-b23c-dfb16c3a1092', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be');

insert into comment values(
	'5602b3ea-cb43-11e9-a39f-2a2ae2dbcfe7',
	'good for you', 
	'19230494-7261-4f7d-98d3-7d6f267b3980', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be',
    false,'5602b3ea-cb45-11e9-a32f-2a2ae2gbcce4');
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-cb45-11e9-a32f-2a2ae2gbcce4') 
	where comment_id = '5602b3ea-cb45-11e9-a32f-2a2ae2gbcce4';

insert into comment values(
	'5602b3ea-cb4l-11e9-a39f-2a2ae2dbcfe7',
	'wish i had a OLED display', 
	'24c011c3-d798-4434-a5d5-32f973acc4ad', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be',
    false,'5602b3ea-cb45-11e9-a32f-2a2ae2gbcce4');
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-cb45-11e9-a32f-2a2ae2gbcce4') 
	where comment_id = '5602b3ea-cb45-11e9-a32f-2a2ae2gbcce4';







insert into comment values(
	'5602b3ea-fb43-11e9-a39f-2a2ae2dbcfe7',
	'First', 
	'9c012fb7-34f2-4b37-b23c-dfb16c3a1092', 
	'dd23e3d8-a1d3-4d08-ac16-b647a974afff');

insert into comment values(
	'5602b3ea-cb45-11e9-a67f-2a2ae2dbcce4',
	'I love planet Earth!!!', 
	'24c011c3-d798-4434-a5d5-32f973acc4ad', 
	'dd23e3d8-a1d3-4d08-ac16-b647a974afff');

insert into comment values(
	'5602b3ea-cb4l-11h6-a39f-2a2ae2dbcfe7',
	'I love UrAnus :)', 
	'19230494-7261-4f7d-98d3-7d6f267b3980', 
	'dd23e3d8-a1d3-4d08-ac16-b647a974afff',
    false,'5602b3ea-cb45-11e9-a67f-2a2ae2dbcce4');
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-cb45-11e9-a67f-2a2ae2dbcce4') 
	where comment_id = '5602b3ea-cb45-11e9-a67f-2a2ae2dbcce4';


insert into comment values(
	'5602b3ea-cb45-11e9-a67f-252ae2dbcce4',
	'Fuck Mars Colony! Love Earth!', 
	'b80b408a-c5c6-4017-8c10-d32f165626b0', 
	'dd23e3d8-a1d3-4d08-ac16-b647a974afff');

insert into comment values(
	'5602b3ea-cb43-11h6-a39f-2a2ae2dbcfe7',
	'lol', 
	'9c012fb7-34f2-4b37-b23c-dfb16c3a1092', 
	'dd23e3d8-a1d3-4d08-ac16-b647a974afff',
    false,'5602b3ea-cb45-11e9-a67f-252ae2dbcce4');
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-cb45-11e9-a67f-252ae2dbcce4') 
	where comment_id = '5602b3ea-cb45-11e9-a67f-252ae2dbcce4';

insert into comment values(
	'5602b3ea-cb43-11h6-a39f-2a2ae3dbcfe7',
	'Yeah, Lets first build a moon base!', 
	'19230494-7261-4f7d-98d3-7d6f267b3980', 
	'dd23e3d8-a1d3-4d08-ac16-b647a974afff',
    false,'5602b3ea-cb45-11e9-a67f-252ae2dbcce4');
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-cb45-11e9-a67f-252ae2dbcce4') 
	where comment_id = '5602b3ea-cb45-11e9-a67f-252ae2dbcce4';


-- insert a root comment
insert into comment values(
	'5602b3ea-fb43-11e9-a71f-2a2ae2dbcfe7',
	'looks great!', 
	'9c012fb7-34f2-4b37-b23c-dfb16c3a1092', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be');

-- insert a non root comment
insert into comment values(
	'5602f3ea-cb43-11h6-a39f-2a2ae3dbcfe7',
	'yup, cant get better than this', 
	'19230494-7261-4f7d-98d3-7d6f267b3980', 
	'ef625eb3-52ab-4ac4-abf8-9501e46324be',
    false,'5602b3ea-fb43-11e9-a71f-2a2ae2dbcfe7');
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-fb43-11e9-a71f-2a2ae2dbcfe7') 
	where comment_id = '5602b3ea-fb43-11e9-a71f-2a2ae2dbcfe7';


-- delete a non root comment
delete from comment where comment_id = '5602f3ea-cb43-11h6-a39f-2a2ae3dbcfe7' and user_id = '19230494-7261-4f7d-98d3-7d6f267b3980';
update comment set no_of_children = 
	(select count(*) from comment where 
	parent_comment_id = '5602b3ea-fb43-11e9-a71f-2a2ae2dbcfe7') 
	where comment_id = '5602b3ea-fb43-11e9-a71f-2a2ae2dbcfe7';

-- delete a root comment
delete from comment where comment_id = '5602b3ea-fb43-11e9-a71f-2a2ae2dbcfe7'
and user_id = '9c012fb7-34f2-4b37-b23c-dfb16c3a1092';

--get comments (roots) by image_id
select comment_id, user_id, username, comment,no_of_children, times_liked, created_on 
from enduser natural join comment 
where image_id = 'ef625eb3-52ab-4ac4-abf8-9501e46324be' 
and is_root = true;

-- get comments (children) by parent_comment_id
select comment_id, user_id, username,comment, no_of_children, times_liked, created_on 
from enduser natural join comment
where parent_comment_id = '5602b3ea-cb43-11e9-a32f-2a2ae2dbcce4'; 

