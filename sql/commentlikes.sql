insert into commentlikes values('5602b3ea-cb45-11e9-a32f-2a2ae2dbcce4',
'b80b408a-c5c6-4017-8c10-d32f165626b0');
update comment set times_liked = 
	(select count(*) from commentlikes where 
	comment_id = '5602b3ea-cb45-11e9-a32f-2a2ae2dbcce4') 
	where comment_id = '5602b3ea-cb45-11e9-a32f-2a2ae2dbcce4';

insert into commentlikes values('5602b3ea-cb45-11e9-a32f-2a2ae2dbcce4',
'19230494-7261-4f7d-98d3-7d6f267b3980');
update comment set times_liked = 
	(select count(*) from commentlikes where 
	comment_id = '5602b3ea-cb45-11e9-a32f-2a2ae2dbcce4') 
	where comment_id = '5602b3ea-cb45-11e9-a32f-2a2ae2dbcce4';

insert into commentlikes values('5602b3ea-cb45-11e9-a32f-2a2ae2dbcce4',
'24c011c3-d798-4434-a5d5-32f973acc4ad');
update comment set times_liked = 
	(select count(*) from commentlikes where 
	comment_id = '5602b3ea-cb45-11e9-a32f-2a2ae2dbcce4') 
	where comment_id = '5602b3ea-cb45-11e9-a32f-2a2ae2dbcce4';

insert into commentlikes values('5502b3ea-cb45-11e9-a32f-2a2ae2gbcce4',
'b80b408a-c5c6-4017-8c10-d32f165626b0');
update comment set times_liked = 
	(select count(*) from commentlikes where 
	comment_id = '5502b3ea-cb45-11e9-a32f-2a2ae2gbcce4') 
	where comment_id = '5502b3ea-cb45-11e9-a32f-2a2ae2gbcce4';

-- like a comment
insert into commentlikes values('5602b3ea-cb45-11e9-a67f-252ae2dbcce4',
'9c012fb7-34f2-4b37-b23c-dfb16c3a1092');
update comment set times_liked = 
	(select count(*) from commentlikes where 
	comment_id = '5602b3ea-cb45-11e9-a67f-252ae2dbcce4') 
	where comment_id = '5602b3ea-cb45-11e9-a67f-252ae2dbcce4';

-- remove like from a comment
delete from commentlikes where comment_id = '5602b3ea-cb45-11e9-a67f-252ae2dbcce4' and user_id = '9c012fb7-34f2-4b37-b23c-dfb16c3a1092';
update comment set times_liked = 
	(select count(*) from commentlikes where 
	comment_id = '5602b3ea-cb45-11e9-a67f-252ae2dbcce4') 
	where comment_id = '5602b3ea-cb45-11e9-a67f-252ae2dbcce4';

