insert into enduser values ('b80b408a-c5c6-4017-8c10-d32f165626b0',
  'theWhale',crypt('67#21785', gen_salt('bf')),'waleatblueocean@gmail.com');

insert into enduser values ('19230494-7261-4f7d-98d3-7d6f267b3980',
  'eggGamerz',crypt('6h5421d4', gen_salt('bf')),'eggGamerz@gmail.com');

insert into enduser values ('9c012fb7-34f2-4b37-b23c-dfb16c3a1092',
  'honeyDeath',crypt('nope9never', gen_salt('bf')),'honeydeathwish@gmail.com');

insert into enduser values ('24c011c3-d798-4434-a5d5-32f973acc4ad',
  'textBird',crypt('tree*3454', gen_salt('bf')),'textbirdy@gmail.com');

insert into enduser values ('56e3f5e6-9bf2-4e92-bf08-81fc57f490c7',
  'fatherHorse',crypt('pasree*345', gen_salt('bf')),'fatherHorse@gmail.com');

insert into enduser values ('257ab809-c83a-4ab1-bf82-91941d8b11c5',
  'redDragon',crypt('red^&*90', gen_salt('bf')),'redDragon145@gmail.com');

/* Queries on the EndUser Relation */

--get enduser
select * from enduser where username = 'whiteShark';

--create enduser
insert into enduser values ('42e80878-fb68-40d2-ac4e-f0b8a8e83228',
  'whiteShark',crypt('white%^56', gen_salt('bf')),'whiteShark65@gmail.com');

-- update enduser
update enduser set username = 'blackShark', email = 'blackShark785@gmail.com'
  where username = 'whiteShark';

-- delete enduser
delete from enduser where username = 'blackShark';

-- login
select username from enduser where username = 'redDragon'
and password = crypt('red^&*90', password);

-- update password
update enduser set password = crypt('yellow^&*90', gen_salt('bf')) where
  username = 'redDragon' and password = crypt('red^&*90', password);
