insert into heart values ('b80b408a-c5c6-4017-8c10-d32f165626b0',
    'ef625eb3-52ab-4ac4-abf8-9501e46324be');
update image set times_marked_fav = (select count(*)
    from heart where image_id = 'ef625eb3-52ab-4ac4-abf8-9501e46324be' ) 
    where image_id = 'ef625eb3-52ab-4ac4-abf8-9501e46324be';
    
insert into heart values ('b80b408a-c5c6-4017-8c10-d32f165626b0',
    '79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe');
update image set times_marked_fav = (select count(*)
    from heart where image_id = '79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe' ) 
    where image_id = '79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe';


insert into heart values('19230494-7261-4f7d-98d3-7d6f267b3980',
    'ef625eb3-52ab-4ac4-abf8-9501e46324be');
update image set times_marked_fav = (select count(*)
    from heart where image_id = 'ef625eb3-52ab-4ac4-abf8-9501e46324be' ) 
    where image_id = 'ef625eb3-52ab-4ac4-abf8-9501e46324be';



-- create heart and also update times_marked_fav in Image
insert into heart values('19230494-7261-4f7d-98d3-7d6f267b3980',
    '79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe');
update image set times_marked_fav = (select count(*)
    from heart where image_id = '79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe') 
    where image_id = '79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe';


-- delete heart and also update times_marked_fav in Image
delete from heart where user_id = '19230494-7261-4f7d-98d3-7d6f267b3980'
    and image_id = '79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe';
update image set times_marked_fav = (select count(*)
    from heart where image_id = '79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe') 
    where image_id = '79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe';

-- get all heart (images) by a user
select * from image natural join heart where user_id = 'b80b408a-c5c6-4017-8c10-d32f165626b0';

-- get times_marked_fav of an image
select count (*)  from heart where image_id = 'ef625eb3-52ab-4ac4-abf8-9501e46324be';