insert into image values ('ef625eb3-52ab-4ac4-abf8-9501e46324be',
  'ac4baada-50d5-4bb2-afd3-3370b9e11b37','abstract_0','abstract multicolor art',
  3.09,4096,3072,'.jpg');

insert into image values ('79c7fb5c-a022-4ed3-bf0b-2b2dc481fcfe',
  'ac4baada-50d5-4bb2-afd3-3370b9e11b37','abstract_1','abstract architecture',
  1.64,2999,1999,'.jpg');

insert into image values ('bf0ea9e6-1d15-4aae-bd95-d153ce355308',
  'ac4baada-50d5-4bb2-afd3-3370b9e11b37','abstract_2','abstract painting',
  0.96,2500,1669,'.jpg');

insert into image values ('e368ac03-7a3d-40da-9404-97af459ae630',
  'ac4baada-50d5-4bb2-afd3-3370b9e11b37','abstract_3','abstract color smoke',
  0.55,2500,1740,'.jpg');

insert into image values ('5291c3f1-ab37-4910-bd44-23d5e4dfeead',
  'ac4baada-50d5-4bb2-afd3-3370b9e11b37','abstract_4','abstract color art ',
  3.04,6000,4000,'.jpg');

insert into image values ('a803eb74-c418-41b7-9566-bd3e9d32a1b6',
  'e83a070d-d350-43d0-8daa-90333fe620ae','earth_0','dry drought earth',
  1.60,3264,2448,'.jpg');

insert into image values ('a5f15e85-52b4-431e-b6bf-3f53ab912d29',
  '12b4b766-22db-481b-80b5-2e1b2570c7c9','earth_1','seashore arielview',
    4.48,3992,2992,'.jpg');

insert into image values ('4ba49de3-28a9-45ce-9b34-eba9236d0af5',
  'e83a070d-d350-43d0-8daa-90333fe620ae','earth_2','earth & astronaut',
  0.83,3032,2008,'.jpg');

insert into image values ('dd23e3d8-a1d3-4d08-ac16-b647a974afff',
  '12b4b766-22db-481b-80b5-2e1b2570c7c9','earth_3','earth from space',
  0.64,2200,1464,'.jpg');

insert into image values ('556e142e-37e5-4afc-9b8c-f37d017651bc',
  'b30710b7-c7c5-4c1f-8abe-12c19117878d','pattern_0','a beautiful pattern',
  3.00,6000,4000,'.jpg');

insert into image values ('723abd2f-eb2b-483c-b57f-0a711e93aba7',
  'b30710b7-c7c5-4c1f-8abe-12c19117878d','pattern_1','hexagon greyscale pattern',
  3.60,3648,2687,'.jpg');

insert into image values ('fc84db2d-33c9-4a9b-af79-b8d95066a5ec',
  'b30710b7-c7c5-4c1f-8abe-12c19117878d','pattern_2','leaf surface pattern',
  3.30,3897,2492,'.jpg');

insert into image values ('b60e488f-44db-4d5b-9428-4fc0a85da94d',
  'b30710b7-c7c5-4c1f-8abe-12c19117878d','pattern_3','triangle greyscale pattern',
  1.85,3000,2000,'.jpg');

insert into image values ('9c1bea84-7041-41f1-81b3-93c63d828049',
  'b30710b7-c7c5-4c1f-8abe-12c19117878d','pattern_4','maze wall pattern',
  2.09,5184,3456,'.jpg');

insert into image values ('362fe098-e131-4c3c-8f85-5c8ea8ed8955',
  'e83a070d-d350-43d0-8daa-90333fe620ae','photo_0','face',
  3.49,5312,2988,'.jpg');

insert into image values ('7ffc886d-56c5-422e-b86a-60bf1278bff0',
  'e83a070d-d350-43d0-8daa-90333fe620ae','photo_1','ariel ocean rock view',
  2.39,3968,2645,'.jpg');

insert into image values ('94aee617-1164-48dc-a102-e80908d5e054',
  'e83a070d-d350-43d0-8daa-90333fe620ae','photo_2','staircase topdown view',
  1.32,2793,2153,'.jpg');

insert into image values ('02d5f345-64cb-4cc8-aa64-ec69be5046f1',
  'e83a070d-d350-43d0-8daa-90333fe620ae','photo_3','hot flowing lava',
  0.79,2400,1600,'.jpg');

insert into image values ('5e381be8-3f88-4887-bb37-0014f5e445aa',
  '12b4b766-22db-481b-80b5-2e1b2570c7c9','space_0','milky way',
  10.0,6070,3414,'.jpg');

insert into image values ('dc052bd7-e351-4a3e-8cf6-bf762192c743',
  '12b4b766-22db-481b-80b5-2e1b2570c7c9','space_1','mars rover',
  0.78,3000,2400,'.jpg');

insert into image values ('ce382b97-a96e-4327-ab11-7a500363f2c3',
  '12b4b766-22db-481b-80b5-2e1b2570c7c9','space_2','moon landing',
  1.78,2700,2700,'.jpg');

insert into image values ('18742dd7-a7d7-4fb1-92d9-3c5ae22ce26c',
  '12b4b766-22db-481b-80b5-2e1b2570c7c9','space_3','interstellar blackhole',
  0.15,1280,720,'.jpg');

insert into image values ('6000db08-f9ea-45e1-9f60-783676cd2084',
  'e83a070d-d350-43d0-8daa-90333fe620ae','texture_0','rockwall texture',
  9.80,5496,3670,'.jpg');

insert into image values ('7ff3b71a-5e73-4e45-8617-ab4850488857',
  'e83a070d-d350-43d0-8daa-90333fe620ae','texture_1','tile rock texture',
  0.44,2500,1717,'.jpg');

insert into image values ('26571422-336c-4b92-92c9-fcd056eb8f9a',
  'e83a070d-d350-43d0-8daa-90333fe620ae','texture_2','wood pieces',
  2.87,4256,2832,'.jpg');


/* Queries on the Image Relational */

-- get image
select * from image where name = 'texture_3';

-- get images of an artist
select * from image where artist_id = (select artist_id from artist join enduser on
artist_id = user_id where username = 'dirtMetal');

-- get images of a Category
select * from image natural join categorization
where cat_id = (select cat_id from category where name = 'abstract');

--create image
insert into image values ('0b88ef60-994f-403c-8b3a-29b6e584009c',
  'e83a070d-d350-43d0-8daa-90333fe620ae','texture_3','tree bark texture',
  4.08,4256,2832,'.jpg');

--update image
update image set name = 'texture_3', description = 'wooden pieces collage' where
  name = 'texture_3';

-- update price of an image
update image set price = 0.5 where name = 'texture_3';

-- delete image
delete from image where name = 'texture_3';

-- get times_marked_fav of an Image
select times_marked_fav from image where name = 'abstract_0';