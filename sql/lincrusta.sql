CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

create table EndUser (
  user_id text,
  username text not null unique,
  password text not null,
  email text unique,
  primary key (user_id)
);

create table Artist (
  artist_id text,
  earnings real default 0 ,
  description text,
  ranking int default -1,
  primary key(artist_id),
  foreign key(artist_id) references EndUser(user_id) on delete cascade,
  constraint positive_earnings check(earnings >= 0)
);

create table Image (
  image_id text,
  artist_id text,
  name text not null unique,
  description text,
  size real,
  width int,
  height int,
  image_format text,
  price real default 0,
  times_marked_fav int default 0,
  times_purchased int default 0,
  primary key (image_id),
  foreign key (artist_id) references Artist(artist_id) on delete cascade
);

create table Category (
  cat_id text,
  name text not null unique,
  description text,
  primary key (cat_id)
);

create table Categorization (
  image_id text,
  cat_id text,
  primary key(cat_id, image_id),
  foreign key(image_id) references Image(image_id) on delete cascade,
  foreign key(cat_id) references Category(cat_id) on delete cascade
);

-- old

create table Collection(
  collection_id text,
  collection_name text not null,
  description text,
  price real default 0,
  primary key(collection_id)
);


create table CollectionMember(
  collection_id text,
  image_id text,
  primary key(collection_id,image_id),
  foreign key(image_id) references Image(image_id) on delete cascade,
  foreign key(collection_id) references Collection(collection_id)
  on delete cascade
);

create table Favourite(
  user_id text,
  item_id text,
  type text,
  primary key(user_id,item_id),
  foreign key(user_id) references EndUser(user_id) on delete cascade,
  foreign key(item_id) references Image(image_id) on delete cascade,
  foreign key(item_id) references Artist(artist_id) on delete cascade,
  foreign key(item_id) references Collection(collection_id) on delete cascade
);

create table Subscription(
  sub_id text,
  user_id text,
  dop date,
  period int,
  state boolean,
  primary key(sub_id),
  foreign key(user_id) references EndUser(user_id) on delete cascade
);

create table Transaction (
  transac_id text,
  user_id text,
  item_id text,
  date date,
  price real,
  type text,
  foreign key(user_id) references EndUser(user_id)on delete cascade,
  foreign key(item_id) references Image(image_id) on delete cascade,
  foreign key(item_id) references Collection(collection_id) on delete cascade,
  foreign key(item_id) references Subscription(sub_id) on delete cascade,
  primary key (transac_id)
);

create table TransacSub (
  transac_id text,
  sub_id text,
  primary key (transac_id, sub_id),
  foreign key (transac_id) references Transaction(transac_id) on delete cascade,
  foreign key (sub_id) references Subscription(sub_id) on delete cascade
);

-- modified


create table Heart(
  user_id text,
  image_id text,
  primary key(user_id,image_id),
  foreign key(user_id) references EndUser(user_id) on delete cascade,
  foreign key(image_id) references Image(image_id) on delete cascade
);

create table Subscribe (
  user_id text,
  artist_id text,
  foreign key(artist_id) references Artist(artist_id)on delete cascade,
  foreign key(user_id) references EndUser(user_id)on delete cascade,
  primary key (user_id,artist_id)
);

create table Comment (
  comment_id text not null,
  comment text not null,
  user_id text not null,
  image_id text not null,
  is_root boolean default true,
  parent_comment_id text,
  no_of_children int default 0,
  times_liked int default 0,
  created_on timestamp default current_timestamp,
  
  primary key(comment_id),
  foreign key(image_id) references Image(image_id) on delete cascade,
  foreign key(user_id) references EndUser(user_id) on delete cascade,
  foreign key(parent_comment_id) references Comment(comment_id) on delete cascade
);

create table CommentLikes(
  comment_id text not null,
  user_id text not null,
  primary key (comment_id, user_id),
  foreign key(user_id) references EndUser(user_id) on delete cascade,
  foreign key(comment_id) references Comment(comment_id) on delete cascade
);