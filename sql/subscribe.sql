insert into subscribe values('b80b408a-c5c6-4017-8c10-d32f165626b0'
    ,'ac4baada-50d5-4bb2-afd3-3370b9e11b37' );

insert into subscribe values('b80b408a-c5c6-4017-8c10-d32f165626b0'
    ,'b30710b7-c7c5-4c1f-8abe-12c19117878d' );

insert into subscribe values('19230494-7261-4f7d-98d3-7d6f267b3980'
    ,'b30710b7-c7c5-4c1f-8abe-12c19117878d' );

insert into subscribe values('19230494-7261-4f7d-98d3-7d6f267b3980'
    ,'e83a070d-d350-43d0-8daa-90333fe620ae' );


-- create subscription
insert into subscribe values('19230494-7261-4f7d-98d3-7d6f267b3980'
    ,'12b4b766-22db-481b-80b5-2e1b2570c7c9' );

-- delete subscription
delete from subscribe where user_id = '19230494-7261-4f7d-98d3-7d6f267b3980' 
and artist_id = '12b4b766-22db-481b-80b5-2e1b2570c7c9';

-- get subscriptions of a particular user
select artist_id from subscribe natural join artist  
        where user_id = 'b80b408a-c5c6-4017-8c10-d32f165626b0';

-- get subscription count of an artist
select count(*) from subscribe where artist_id = 'b30710b7-c7c5-4c1f-8abe-12c19117878d';