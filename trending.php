<html>
    <head>
		<link rel="stylesheet" type="text/css" href="css/imagefeed.css">
		<link rel="stylesheet" type="text/css" href="css/nav.css">		
    </head>
    <body>
        <?php 
            session_start();
            include dirname(__FILE__,1).'\utils\latest_response.php';  
			require dirname(__FILE__,1).'\utils\image_utils.php'; 
			include dirname(__FILE__,1).'\utils\nav_from_root.html'; 
        ?>

		<?php 
			$result = get_random_images(10);
			if ($_SESSION['status'] == 1){
				
				$str = '';

				while($row = pg_fetch_assoc($result)){
					$image_name = $row['name'];
					$image_width = $row['width'];
					$image_height = $row['height'];
					$image_format = $row['image_format'];

					while ($image_width > 1000){
						$image_width /= 2;
						$image_height /= 2;
					}

					$file_loc = '\\'.pathinfo(dirname(__FILE__,1))['basename'].'\\uploads\\images\\'.$image_name.$image_format;
					$str.= 
						'<div class= parent>
							<div class=picture>
								<img src="'. $file_loc .'" width="' .($image_width).'" height="'.($image_height). '">
							</div>
						</div>';
				}

				echo $str;

			}
		?>
    </body>
</html>