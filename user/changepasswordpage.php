<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/nav.css">
	<link rel="stylesheet" type="text/css" href="../css/form.css">
	<title> Change Password </title>
</head>
<body>
	<?php
		require dirname(__FILE__,2).'\utils\logged_in.php';
		include dirname(__FILE__,2).'\utils\nav.html'; 
	?>
	<div class= container>
	<form action="update_password.php" method='post'>
		<fieldset>
		<legend> Change Password </legend>
		Old Password:<br>
 	 	<input type="password" name="old_password">
		<br>
		New Password:<br>
 	 	<input type="password" name="new_password">
		<br><br>
  		<input type="submit" value="Submit">
</form>
</fieldset>
</div>
</body>
