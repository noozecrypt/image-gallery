<?php
require dirname(__FILE__,2).'\utils\dbheader.php';
require dirname(__FILE__,2).'\utils\user_utils.php';
require dirname(__FILE__,2).'\utils\utils.php';
session_start();

$username;
$user_id;

init();
delete();

function init(){
    global $username;
    global $user_id;

    $username = $_SESSION['logged_in'];
    $user_id = get_uuid_user();
    if ($_SESSION['status'] == 0){
        delete_failure();
    }
}


function delete(){
    global $conn;
    global $username;
    global $user_id;

    $sql = "delete from enduser where username = '$username' and user_id = '$user_id'";  
    $result = pg_query($conn, $sql); 
    if (!$result || pg_affected_rows($result) === 0){
        delete_failure();
    }else {
        delete_success();
    }
}


function delete_failure(){
  on_failure('account deletion failed!', '/user/usersettingspage.php');
}

function delete_success(){
    on_success('account deletion success!', '/user/logout.php');
}
?>
