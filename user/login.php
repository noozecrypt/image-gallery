<?php
require dirname(__FILE__,2).'\utils\dbheader.php'; 
require dirname(__FILE__,2).'\utils\user_utils.php';
require dirname(__FILE__,2).'\utils\utils.php';

session_start();

# replace with form data or AJAX request data
$username;
$password;
$is_keep_me_loggedin;

init();
login();

function init(){
    global $username;
    global $password;
    global $is_keep_me_loggedin;
    /*
    $username = "whiteShark";
    $password = "white%^56";*/

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username =  $_POST["username"];
        $password = $_POST["password"];
    }

    $is_keep_me_loggedin = true;
}

function login(){
    global $username;
    global $password;
    global $conn;
    
    $sql = "select username from enduser where username = '$username'
    and password = crypt('$password', password)";  
    $result = pg_query($conn, $sql); 

    if (!$result || pg_num_rows($result) == 0) {
        login_failure();
    }else {
        login_success();
    }
}


function login_failure(){
    $_SESSION["logged_in"] = 0; 
    on_failure('login failure!', '/user/loginpage.php');
}


function login_success(){
    global $username;
    global $is_keep_me_loggedin;

    $_SESSION["logged_in"] = $username;

    if ($is_keep_me_loggedin){
        set_user_cookie();
    }

    on_success('login success!', '/home.php');
}


function set_user_cookie() {
    $cookie_name = 'userID';
    global $username;
    $cookie_value = get_user_uuid($username);
    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/" );
}

?>