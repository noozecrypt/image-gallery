<?php

require dirname(__FILE__,2).'\utils\utils.php';

session_start();

logout();

function logout(){
    $_SESSION["logged_in"] = 0;
    delete_user_cookie();
    on_success('user logged out!', '/user/loginpage.php');
}

function delete_user_cookie() {
    $cookie_name = "userID";
    $cookie_value = "";
    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/" );
}
?>