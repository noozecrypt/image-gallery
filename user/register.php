<?php
require dirname(__FILE__,2).'\utils\dbheader.php';
require dirname(__FILE__,2).'\utils\user_utils.php';
require dirname(__FILE__,2).'\utils\utils.php';

session_start();

define("ENDUSER", "enduser");
define("ARTIST", "artist");

$username;
$password;
$email;
$uuid;
$desc;
$user_type;

init();
reg();

function init(){
    global $username;
    global $password;
    global $email;
    global $uuid;
    global $desc;
    global $user_type;

    $uuid = gen_uuid();
    /*$username = "whiteShark";
    $password = "white%^56";
    $email =  "whiteShark65@gmail.com";
    $user_type = ARTIST;
    $desc = "abstract painter";*/

    $desc = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username =  $_POST["username"];
        $password = $_POST["password"];
        $email = $_POST["email"];
        $user_type = $_POST["type"];
    }

    
}


function reg() {
    global $conn;
    global $username;
    global $password;
    global $email;
    global $uuid;
    global $desc;
    global $user_type;

    $sql = "BEGIN";
    $result = pg_query($conn, $sql); 

    if (!$result){
        reg_fail();
    }


    $sql = "insert into enduser values ('$uuid',
    '$username',crypt('$password', gen_salt('bf')),'$email')";  
    $result = pg_query($conn, $sql); 
    if (!$result || pg_affected_rows($result) === 0){
        reg_fail();
    }


    if ($user_type == ARTIST){
        
        $sql = "insert into artist (artist_id, description)
            values ('$uuid', '$desc')";
        $result = pg_query($conn, $sql);
        if (!$result || pg_affected_rows($result) === 0){
            $sql = "ROLLBACK";
            $result = pg_query($conn, $sql); 
            reg_fail();
        }
    }

    $sql = "COMMIT";
    $result = pg_query($conn, $sql); 
    if (!$result){
        reg_fail();
    }

    reg_success();
}


function reg_success(){
    on_success('registeration success!', '/user/loginpage.php');
}


function reg_fail(){
    on_failure('registeration fail!', '/user/registerpage.php');
}
        
?>
