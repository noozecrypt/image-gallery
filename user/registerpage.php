<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../css/nav.css">
        <link rel="stylesheet" type="text/css" href="../css/form.css">	
    </head>
    <body>
        <?php
            session_start(); 
            
            include dirname(__FILE__,2).'\utils\latest_response.php';  
            include dirname(__FILE__,2).'\utils\nav.html'; 

            define("ENDUSER", "enduser");
            define("ARTIST", "artist");
        ?>
        <div class= container>
         <form action="register.php" method = 'post'>
             <fieldset>
                 <legend> Register </legend>
  	 	    Username:<br>
 	 	    <input type="text" name="username">
 	 	    <br>
		    Password:<br>
 	 	    <input type="password" name="password">
            <br>
            Repeat Password:<br>
 	 	    <input type="password" name="repeat_password">
		    <br>
            Email:<br>
 	 	    <input type="email" name="email">
 	 	    <br>
            Type:<br>
 	 	    <input type="radio" name="type" value = "enduser">User<br>
            <input type="radio" name="type" value = "artist">Artist
            <br><br>
              <input type="submit" value="Submit">
</fieldset>
        </form> 
</div>
    </body>
</html>