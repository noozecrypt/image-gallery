<?php
    require dirname(__FILE__,2).'\utils\dbheader.php';
    require dirname(__FILE__,2).'\utils\user_utils.php';
    require dirname(__FILE__,2).'\utils\utils.php';

    session_start();
    
    $artist_name;
    $user_id;
    $artist_id;
    
    init();
    unsub_artist();

    #for debug only
    include dirname(__FILE__,2).'\utils\latest_response.php';

    function init(){
        global $artist_name;
        global $user_id;
        global $artist_id;

        $artist_name = 'greenBinary';

        $user_id = get_uuid_user();
        if ($_SESSION['status'] == 0){
            unsub_fail();
        }

        $artist_id = get_user_uuid($artist_name);
        if ($_SESSION['status'] == 0){
            unsub_fail();
        }
    }


    function unsub_artist(){
        global $conn;
        global $user_id;
        global $artist_id;

        $sql = "delete from subscribe where user_id = '$user_id' 
        and artist_id = '$artist_id'";
        $result = pg_query($conn, $sql);
        if (!$result || pg_affected_rows($result) == 0){
            unsub_fail();
        }else{
            unsub_success();
        }

    }
   

    function unsub_fail(){
      on_failure('unsubscribe fail!', 0);
    }

    function unsub_success(){
        on_success('unsubscribe success!', 0);
    }    
        
?>