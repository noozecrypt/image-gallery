<?php
require dirname(__FILE__,2).'\utils\dbheader.php';
require dirname(__FILE__,2).'\utils\user_utils.php';
require dirname(__FILE__,2).'\utils\utils.php';

session_start();

$artist_id;
$new_desc;

init();
update();

function init(){
    global $artist_id;
    global $new_desc;

    $new_desc = "vector artist";
 
    $artist_id = get_uuid_user();
    if ($_SESSION['status'] == 0){
        update_fail();
    }
}

function update(){
    global $conn;
    global $artist_id;
    global $new_desc;

    $sql = "update artist set description = '$new_desc' where artist_id ='$artist_id'";
    $result = pg_query($conn, $sql);
    if (!$result || pg_affected_rows($result) === 0){
        update_fail();
    }else {        
        update_success();
    }

}

function update_success(){
    on_success('artist desc update success!', '/user/usersettingspage.php');
}

function update_fail(){
    on_failure('artist desc update failure!', '/user/usersettingspage.php'); 
}
        
?>
