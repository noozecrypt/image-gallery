<?php
require dirname(__FILE__,2).'\utils\dbheader.php';
require dirname(__FILE__,2).'\utils\user_utils.php';
require dirname(__FILE__,2).'\utils\utils.php';

session_start();

$new_email;
$username;
$user_id;

init();
update();

function init(){
    global $new_email;
    global $username;
    global $user_id;

    $username = $_SESSION['logged_in'];
    
    /*$new_email = 'blackShark45@gmail.com';*/

    $user_id = get_uuid_user();
    if ($_SESSION['status'] == 0){
        update_fail();
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $new_email =  $_POST["new_email"];
    }
        
}

function update(){
    global $conn;
    global $new_email;
    global $username;
    global $user_id;

    $sql = "update enduser set email = '$new_email'
        where username = '$username' and user_id = '$user_id'";  
    $result = pg_query($conn, $sql); 
    if (!$result || pg_affected_rows($result) == 0){
        update_fail();
    }else{
        update_success();
    }

}


function update_success(){
   on_success('email update success!', '/user/usersettingspage.php');
}


function update_fail(){
    on_failure('email update failure', '/user/usersettingspage.php');
}
        
?>
