<?php
require dirname(__FILE__,2).'\utils\dbheader.php';
require dirname(__FILE__,2).'\utils\user_utils.php';
require dirname(__FILE__,2).'\utils\utils.php';

session_start();

$user_id;
$username;
$old_password;
$new_password;


init();
update();


function init(){
    global $user_id;
    global $username;
    global $old_password;
    global $new_password;
    /*
    $old_password = "white%^56";
    $new_password =  "yellow^&*90";
    */
    $username = $_SESSION['logged_in'];
    $user_id = get_uuid_user();
    if ($_SESSION['status'] == 0){
        update_fail();
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $old_password =  $_POST["old_password"];
        $new_password =  $_POST["new_password"];
    }
    
}

function update(){
    global $conn;
    global $user_id;
    global $username;
    global $old_password;
    global $new_password;

    $sql = "update enduser set password = crypt('$new_password', gen_salt('bf')) where
    username = '$username' and password = crypt('$old_password', password) and user_id = '$user_id'";  
    $result = pg_query($conn, $sql); 
    if (!$result || pg_affected_rows($result) == 0){
        update_fail();
    }else{
        update_success();
    }

}

function update_success(){
    on_success('password update success!', '/user/logout.php');
 }
 
 
 function update_fail(){
     on_failure('password update failure', '/user/usersettingspage.php');
 }
        
?>
