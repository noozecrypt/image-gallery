<?php
require dirname(__FILE__,2).'\utils\dbheader.php';
require dirname(__FILE__,2).'\utils\user_utils.php';
require dirname(__FILE__,2).'\utils\utils.php';

session_start();

$new_username;
$old_username;
$password;
$user_id;    

init();
update();

function init(){
    global $new_username;
    global $old_username;
    global $password;
    global $user_id;    

    /*
    $new_username =  'blackShark';
    $password = "white%^56";
    */

    $old_username = $_SESSION['logged_in'];

    $user_id = get_uuid_user();
    if ($_SESSION['status'] == 0){
        update_fail();
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $new_username =  $_POST["new_username"];
        $password = $_POST["password"];
    }
}


function update(){
    global $conn;
    global $new_username;
    global $old_username;
    global $password;
    global $user_id;   

    $sql = "update enduser set username = '$new_username' where
        username = '$old_username' and password = crypt('$password', password) and user_id = '$user_id'";  
    $result = pg_query($conn, $sql); 
    if (!$result || pg_affected_rows($result) == 0){
        update_fail();
    }else {
        update_success();
    }
}


function update_success(){
    on_success('username update success!','/user/logout.php' );
}


function update_fail(){
    on_success('username update failure!','/user/usersettingspage.php' );
}
        
?>
