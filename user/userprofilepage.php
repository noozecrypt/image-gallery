<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../css/nav.css">
        <link rel="stylesheet" type="text/css" href="../css/form.css">
    </head>
    <body>
        <?php 
            session_start();

            require dirname(__FILE__,2).'\utils\logged_in.php'; 
            include dirname(__FILE__,2).'\utils\nav.html'; 

            $username = get_username();
            $result = get_user($username);
            $row = pg_fetch_assoc($result);
            $email = $row['email'];
            $desc = '';
            $flag = false;

            if (is_artist($username)){
                
                if (isset($row['description'])){
                    $desc = $row['description'];
                    $flag = true;
                }
                   
            }

            /*

            $user_name = 'whiteShark';
            $result = get_user($user_name);
            printout($result);

            $artist_name = 'whiteShark';
            $result = get_artist($artist_name);
            printout($result);

            $user_name = 'theWhale';
            $user_id = get_user_uuid($user_name);
            if ($_SESSION['status'] == 1){
                $result = get_subscriptions($user_id);
                if ($_SESSION['status'] == 1){
                    for ($x = 0; $x < sizeof($result); $x ++){
                        echo $result[$x];
                    }
                }
            }

            */

            $str = '<div class = container>
            <fieldset><legend>User Details</legend>
            Username: '.$username.' <br>
            Email: '.$email.' <br>';

            if ($flag){
                $str.='Description: '.$desc. '<br>';
            }

            $str.= '</fieldset></div>';

            echo $str;

            include dirname(__FILE__,2).'\utils\latest_response.php'; 
        ?>

 
        </div>

        /* add html to  display user profile or artist profile info */
    </body>
</html>