<html>
    <head>
    <style>


		.header {
				width:500px;
				font-family: sans-serif;
				font-size: 50px;
				border: 5px solid #ccc;
				padding: 25px;
				text-align: center;
				color: white;
				background: url(../css/assets/Untitled.png);
				border-style: solid;
				border-radius: 25px;
				
			}	

		.menu {
			margin: 0;
    		padding: 0;
			overflow: hidden;
		}

		.container {
			margin:32px;
    		width: 1000px;
    		margin-left:auto;
    		margin-right:auto;
   	 		text-align:center;
		}

		.button {
				background-color: black;
				display: inline-block;
				float:left;
				opacity: 0.80;
				border-style: solid;		
				
			}
		.button a {
				font-size: 30px;
				display: inline-block;
				padding: 15px 25px;
				color: white;
				text-decoration: none;
				text-align: center;
				
			  }
		.button a:hover {
			background-color: green;	
		}		
				
		body { 
			background: url(../css/assets/crysistest.jpg);
			background-size: cover;
		    }
		</style>
		<link rel="stylesheet" type="text/css" href="../css/nav.css">	
    </head>
    <body>
        <?php 
            session_start();

            include dirname(__FILE__,2).'\utils\latest_response.php'; 
			require dirname(__FILE__,2).'\utils\logged_in.php';
			include dirname(__FILE__,2).'\utils\latest_response.php'; 
			include dirname(__FILE__,2).'\utils\nav.html'; 
			

        ?>

    
	<div class = 'container' style="width:500px;">
		<div class= header> User Settings </div> 
	</div>
	<div class = menu>
		<div class='container'>
			<div class=button>
			 	<a href="changepasswordpage.php"> Change Password </a> 
			 	<a href="changeusernamepage.php"> Change Username </a> 
			 	<a href="changeemailpage.php"> Change E-mail </a>
			 	<a href="delete_user.php"> Delete User </a>
			</div>
		</div>	
	</div>
    </body>
</html>