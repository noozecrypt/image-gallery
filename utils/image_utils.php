<?php
require dirname(__FILE__,2).'\utils\dbheader.php'; 


function get_image($image_name){
    global $conn;

    $sql = "select * from image where name = '$image_name'";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return $result;
    }
}

function get_images($artist_name){
    global $conn;

    $sql = "select * from image where artist_id = (select artist_id from artist join enduser on
    artist_id = user_id where username = '$artist_name')";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return $result;
    }
}

function get_image_by_cat($cat_name){
    global $conn;

    $sql = "select * from image natural join categorization
    where cat_id = (select cat_id from category where name = '$cat_name')";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return $result;
    }
}

function get_category($cat_name){
    global $conn;

    $sql = "select * from category where name = '$cat_name'";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return $result;
    }
}

function get_image_uuid($image_name){
    global $conn;

    $sql = "select image_id from image where name = '$image_name'";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return pg_fetch_assoc($result)['image_id'];
    }
}


function get_cat_uuid($cat_name){
    global $conn;

    $sql = "select cat_id from category where name = '$cat_name'";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return pg_fetch_assoc($result)['cat_id'];
    }
}

function get_artist_uuid($image_name){
    global $conn;

    $sql = "select artist_id from image where name= '$image_name'";
    
    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return pg_fetch_assoc($result)['artist_id'];
    }

}

function user_hearts($user_id){
    global $conn;

    $sql = "select * from image natural join heart 
        where user_id = '$user_id'";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return $result;
    }

}

function times_marked_fav($image_name){
    global $conn;

    $sql = "select times_marked_fav from image where name = '$image_name';";
    
    $result = pg_query($conn, $sql); 
    if (!$result || pg_affected_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return pg_fetch_assoc($result)['times_marked_fav'];
    }
}

function get_image_format($image_name){
    global $conn;

    $sql = "select image_format from image where name = '$image_name'";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return pg_fetch_assoc($result)['image_format'];
    }
}

function get_root_comments($image_id){
    global $conn;

    $sql = "select comment_id, user_id, username, comment,no_of_children, times_liked, created_on 
    from enduser natural join comment 
    where image_id = '$image_id' 
    and is_root = true";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return $result;
    }

}

function get_comments($parent_comment_id){
    global $conn;

    $sql = "select comment_id, user_id, username,comment, no_of_children, times_liked, created_on 
    from enduser natural join comment
    where parent_comment_id = '$parent_comment_id'; ";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return $result;
    }

}

function get_random_images($limit) {
    global $conn;

    $sql = "select * from image order by random() limit $limit";

    $result = pg_query($conn, $sql); 
    if (!$result || pg_num_rows($result) === 0){
        $_SESSION['status'] = 0;
        return null;
    }else {
        $_SESSION['status'] = 1;
        return $result;
    }
    

}



?>