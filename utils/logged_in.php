<?php
require dirname(__FILE__,2).'\utils\dbheader.php';    
require dirname(__FILE__,2).'\utils\user_utils.php';
require dirname(__FILE__,2).'\utils\utils.php';

is_logged_in();

function is_logged_in(){
    $cookie_name = 'userID';
    if(!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] === 0 ){
        if (isset($_COOKIE[$cookie_name])){
            logged_in();
        }else{
            login_failure();
        }
    }else{
        logged_in();
    }
}


function login_failure(){
    
    $path = '/user/loginpage.php';

    if (isset($_SESSION['redirect'])){
        $path = $_SESSION['redirect'];
        unset($_SESSION['redirect']);
    }

    on_failure('login fail!',$path);

}

function logged_in(){
    $_SESSION["logged_in"] = get_username();
    if ($_SESSION['status']==1){
        on_success('logged in!', 0);
    }else{
        login_failure();
    }   
}

?>