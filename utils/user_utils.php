<?php
    require dirname(__FILE__,2).'\utils\dbheader.php'; 

    function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
     
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),
     
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,
     
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
     
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }


    function get_user_uuid($username){
        global $conn;
        $sql = "select user_id from enduser where username = '$username'";
    
        $result = pg_query($conn, $sql);
    
        if (!$result || pg_num_rows($result) == 0){
            $_SESSION['status'] = 0;
            return "";
        }else{
            $_SESSION['status'] = 1;
            $row = pg_fetch_assoc($result);
            return $row['user_id'];
        }
    
    }

    function get_username(){
        global $conn;
        $cookie_name = 'userID';

        if (!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] == 0 ){
            if (isset($_COOKIE[$cookie_name])){
                $sql = "select username from enduser where user_id = '$_COOKIE[$cookie_name]'";
        
                $result = pg_query($conn, $sql);
        
                if (!$result || pg_num_rows($result) == 0){
                    $_SESSION['status'] = 0;
                    return "";
                }else{
                    $_SESSION['status'] = 1;
                    $row = pg_fetch_assoc($result);
                    return $row['username'];
                }        
                
            }else {
                $_SESSION['status'] = 0;
            }
        }else {
            return $_SESSION['logged_in'];
        }        
    
    }

    function get_user($username) {
        global $conn;

        $sql = "select * from enduser where username = '$username'";

        $result = pg_query($conn, $sql); 
        if (!$result || pg_num_rows($result) === 0){
            $_SESSION['status'] = 0;
            return null;
        }else {
            $_SESSION['status'] = 1;
            return $result;
        }
    }      

    function get_artist($username) {
        global $conn;

        $sql = "select * from enduser join artist on user_id = artist_id
        where username = '$username'";

        $result = pg_query($conn, $sql); 
        if (!$result || pg_num_rows($result) === 0){
            $_SESSION['status'] = 0;
            return null;
        }else {
            $_SESSION['status'] = 1;
            return $result;
        }
    }
    
    function is_artist($username){
        global $conn;

        $sql = "select * from enduser join artist on user_id = artist_id
        where username = '$username'";

        $result = pg_query($conn, $sql); 
        if (!$result || pg_num_rows($result) === 0){
            return false;
        }else {
            return true;
        }
    }

    function get_subscriptions($user_id){
        global $conn;
        
        $sql = "select artist_id from subscribe natural join artist  
        where user_id = '$user_id'";

        $result = pg_query($conn, $sql); 
        if (!$result || pg_num_rows($result) === 0){
            $_SESSION['status'] = 0;
            return null;
        }else {
            $_SESSION['status'] = 1;
            $a = array();
            while ($row = pg_fetch_assoc($result)){
                $res = get_username_by_uuid($row['artist_id']);
                if ($_SESSION['status'] == 1){
                    array_push($a, $res);
                }else{
                    $_SESSION['status'] = 0;
                    return null;
                }
                
            }
            return $a;
        }

    }

    function get_uuid_user(){
        $cookie_name = 'userID';
        $user_id;
        if (isset($_COOKIE[$cookie_name])){
            $user_id = $_COOKIE[$cookie_name];
        }else{
            $user_id = get_user_uuid($_SESSION['logged_in']);
        }
        return $user_id;
    }

    function get_username_by_uuid($user_id){
        global $conn;

        $sql = "select username from enduser where user_id = '$user_id'";
         $result = pg_query($conn, $sql); 
        if (!$result || pg_num_rows($result) === 0){
            $_SESSION['status'] = 0;
            return null;
        }else {
            $_SESSION['status'] = 1;
            $row = pg_fetch_assoc($result);
            return $row['username'];
        }

    }
?>