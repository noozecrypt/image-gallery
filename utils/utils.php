<?php

    function printout($result){
        if ($_SESSION['status'] === 1){
            while($row = pg_fetch_assoc($result)){
                foreach ($row as $k => $v) {
                    echo "[$k] => $v.<br>";
                }
                echo "<br><br>";
            }
        }else {
            echo "Fail";
        }
    }

    function on_success($latest_response, $redirect_path){
        $_SESSION["latest_response"] = $latest_response;
        $_SESSION['status'] = 1;
        redirect($redirect_path);
        
    }

    function on_failure($latest_response, $redirect_path){
        $_SESSION["latest_response"] = $latest_response;
        $_SESSION['status'] = 0;
        redirect($redirect_path);
    }

    function redirect($redirect_path){
        if ($redirect_path !== 0){
            $redirect = 'http://localhost:80/'.pathinfo(dirname(__FILE__,2))['basename'].$redirect_path;
            header("Location: $redirect"); 
            exit;
        }
        
    }

?>